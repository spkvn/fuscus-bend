package org.vhom.fuscus.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Internet;
import com.github.javafaker.Name;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.vhom.fuscus.controllers.AuthController;
import org.vhom.fuscus.model.User;
import org.vhom.fuscus.util.JWTHandler;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.github.javafaker.Faker;


@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = {"org.vhom.fuscus"})
@AutoConfigureMockMvc
public class AuthControllerTest {

    private Faker f;
    AuthControllerTest(){
        f = new Faker();
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private User fakeUser(){
        User u = new User();
        Name n = f.name();
        Internet i = f.internet();
        u.setUsername(n.username());
        u.setFirstName(n.firstName());
        u.setLastName(n.lastName());
        u.setPassword(i.password());
        u.setEmail(i.safeEmailAddress());
        return u;
    }

    private User fakeUser(String password) {
        User u = new User();
        Name n = f.name();
        Internet i = f.internet();
        u.setUsername(n.username());
        u.setFirstName(n.firstName());
        u.setLastName(n.lastName());
        u.setPassword(password);
        u.setEmail(i.safeEmailAddress());
        return u;
    }

    @Autowired
    private MockMvc mvc;

    @Test
    public void onRegisterGetRegisterResponse () throws Exception {
        User u = fakeUser();
        MvcResult res = mvc.perform(post("/register")
          .content(asJsonString(u))
          .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.user.username").value(u.getUsername()))
          .andReturn();
    }

    @Test
    public void onAuthenticateGetAuthResponse() throws Exception {
        String pass = "passpass";
        User u = fakeUser(pass);
        String authRequestString = "{" +
                "\"username\": \"" + u.getUsername() + "\"," +
                "\"password\": \"" + pass +
            "\"}";
        // register with user (how to mock this?)
        mvc.perform(post("/register")
            .content(asJsonString(u))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        mvc.perform(post("/authenticate")
            .content(authRequestString)
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isOk());
    }

}
