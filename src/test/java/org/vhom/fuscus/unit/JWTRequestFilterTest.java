package org.vhom.fuscus.unit;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.vhom.fuscus.config.JWTRequestFilter;


@RunWith(SpringRunner.class)
public class JWTRequestFilterTest {

    JWTRequestFilter filter = new JWTRequestFilter();

    @Test
    void testTokenExtractsHeaderWithCorrectFormat() {
        String token = "neworderisbloodysicktobehonest";
        String header = String.format("Bearer %s", token);
        String extractedToken = filter.getTokenFromHeaderString(header);

        assertEquals(extractedToken, token);
    }

    @Test
    void testTokenFailsToExtractHeaderWithIncorrectFormat(){
        String header = "dkfjgksdhgls";
        String extractedToken = filter.getTokenFromHeaderString(header);

        assertNull(extractedToken);
    }
}
