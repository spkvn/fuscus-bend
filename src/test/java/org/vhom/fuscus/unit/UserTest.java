package org.vhom.fuscus.unit;


import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.vhom.fuscus.model.User;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
public class UserTest {
    @Test
    public void whenSettingsPropertiesTheyAreCorrect(){
        User u = new User();
        u.setId(0);
        u.setFirstName("Kevin");
        u.setLastName("M");
        u.setEmail("kevin.m@gmail.com");
        u.setPassword("passpass");
        u.setUsername("keviniscool");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime later = LocalDateTime.now().plusMinutes(30);
        u.setCreatedAt(now);
        u.setUpdatedAt(later);

        assertEquals(u.getId(), 0l);
        assertEquals(u.getFirstName(), "Kevin");
        assertEquals(u.getLastName(), "M");
        assertEquals(u.getEmail(), "kevin.m@gmail.com");
        assertEquals(u.getPassword(), "passpass");
        assertEquals(u.getUsername(), "keviniscool");
        assertEquals(u.getCreatedAt(), now);
        assertEquals(u.getUpdatedAt(), later);
    }
}
