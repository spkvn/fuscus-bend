CREATE TABLE day_exercises (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    day_id INT NOT NULL,
    exercise_id INT NOT NULL,
    measurement_id INT NOT NULL,
    sets INT,
    repetitions INT,
    `order` INT NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (day_id) REFERENCES days(id),
    FOREIGN KEY (exercise_id) REFERENCES exercises(id),
    FOREIGN KEY (measurement_id) REFERENCES measurements(id)
);