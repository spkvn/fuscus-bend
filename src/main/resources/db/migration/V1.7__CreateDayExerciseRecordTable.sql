CREATE TABLE day_exercise_record (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    day_record_id INT NOT NULL,
    day_exercise_id INT NOT NULL,
    set_number INT,
    repetitions INT,
    measurement INT,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (day_record_id) REFERENCES day_records(id),
    FOREIGN KEY (day_exercise_id) REFERENCES day_exercises(id)
);