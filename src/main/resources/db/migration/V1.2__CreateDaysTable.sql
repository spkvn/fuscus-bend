CREATE TABLE days (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    program_id int NOT NULL,
    name VARCHAR(255),
    `order` INT NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (program_id) REFERENCES programs(id)
);