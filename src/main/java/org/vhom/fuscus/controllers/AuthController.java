package org.vhom.fuscus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.vhom.fuscus.model.User;
import org.vhom.fuscus.request.JWTRequest;
import org.vhom.fuscus.responses.JWTResponse;
import org.vhom.fuscus.responses.RegisterResponse;
import org.vhom.fuscus.util.JWTHandler;
import org.vhom.fuscus.util.JWTUserDetailService;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private JWTHandler jwtHandler;

    @Autowired
    private JWTUserDetailService userDetailService;


    @RequestMapping(path="/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthToken(@RequestBody JWTRequest request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        UserDetails userDetails = userDetailService.loadUserByUsername(request.getUsername());
        String token = jwtHandler.generateToken(userDetails);
        return ResponseEntity.ok(new JWTResponse(token));
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> register(@RequestBody User user) throws Exception {
        user = userDetailService.save(user);
        UserDetails userDetails = userDetailService.loadUserByUsername(user.getUsername());
        String token = jwtHandler.generateToken(userDetails);
        return ResponseEntity.ok(new RegisterResponse(user, token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        } catch (Exception e) {
            System.err.println(e.toString());
            e.printStackTrace();
        }
    }
}
