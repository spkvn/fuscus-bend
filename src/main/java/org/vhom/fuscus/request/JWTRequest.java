package org.vhom.fuscus.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor @Getter @Setter @AllArgsConstructor
public class JWTRequest implements Serializable {

    private String email;
    private String username;
    private String password;

}
