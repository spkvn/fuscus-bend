package org.vhom.fuscus.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.vhom.fuscus.model.User;
import org.vhom.fuscus.respositories.UserRepository;

import java.util.ArrayList;

@Service
public class JWTUserDetailService implements UserDetailsService {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findUserFromAuth(Authentication auth) {
        UserDetails ud = (UserDetails) auth.getPrincipal();
        return userRepo.findByUsername(ud.getUsername());
    }

    public User findByUsername(String username) {
        User u = userRepo.findByUsername(username);
        if (u == null) {
            throw new UsernameNotFoundException("User not found with username of :" + username);
        }
        return u;
    }

    public User findByEmail(String username) {
        User u = userRepo.findByEmail(username);
        if (u == null) {
            throw new UsernameNotFoundException("User not found with username of :" + username);
        }
        return u;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User u = findByUsername(username);
        return new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), new ArrayList<>());
    }

    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        User u = findByEmail(email);
        return new org.springframework.security.core.userdetails.User(u.getEmail(), u.getPassword(), new ArrayList<>());
    }

    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepo.save(user);
    }
}
