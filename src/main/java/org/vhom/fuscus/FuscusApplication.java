package org.vhom.fuscus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuscusApplication {

    public static void main(String[] args) {
        SpringApplication.run(FuscusApplication.class, args);
    }

}
