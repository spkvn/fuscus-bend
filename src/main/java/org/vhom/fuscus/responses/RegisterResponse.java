package org.vhom.fuscus.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.vhom.fuscus.model.User;

@AllArgsConstructor @Getter @Setter
public class RegisterResponse {
    private User user;
    private String token;
}
